package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXunitest {
    
    public OXunitest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
 
    public void checkwinner_O_Vetiacal_1_1_Output_True(){
      char[][] table = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
      char currentplayer = 'O';
       boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(true, result);
         
      }
     @Test
      public void checkwinner_X_Vetiacal_1_1_Output_True(){
      char[][] table = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
      char currentplayer = 'X';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(true, result);
      }
      
    @Test
      public void checkwinner_X_Vetiacal_1_1_Output_False(){
      char[][] table = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
      char currentplayer = 'X';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(false, result);
         
      }
      @Test
      public void checkwinner_O_Vetiacal_1_1_Output_False(){
      char[][] table = {{'O', '-', 'O'}, 
                       {'-', 'O', '-'}, 
                       {'-', '-', '-'}};
      char currentplayer = 'O';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(false, result);
         
      }
      @Test
      public void checkwinner_O_Horizon_1_1_Output_True(){
      char[][] table = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
      char currentplayer = 'O';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(true, result);
         
      }
       @Test
      public void checkwinner_X_Horizon_1_1_Output_false(){
      char[][] table = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
      char currentplayer = 'X';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(false, result);
         
      }
      @Test
      public void checkwinner_O_Diagonal_1_Output_true(){
      char[][] table = {{'O', '-', '-'},
                       {'-', 'O', '-'}, 
                       {'-', '-', 'O'}};
      char currentplayer = 'O';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(true, result);
         
      }
       @Test
      public void checkwinner_X_Diagonal_2_Output_true(){
      char[][] table = {{'-', '-', 'X'},
                       {'-', 'X', '-'}, 
                       {'X', '-', '-'}};
      char currentplayer = 'X';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(true, result);
         
      }
       @Test
      public void checkwinner_O_Diagonal_2_Output_false(){
      char[][] table = {{'-', '-', 'X'},
                       {'-', 'O', '-'}, 
                       {'O', '-', '-'}};
      char currentplayer = 'O';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(false, result);
         
      }
       @Test
      public void checkwinner_X_Diagonal_2_Output_false(){
      char[][] table = {{'O', '-', '-'},
                       {'O', '-', '-'}, 
                       {'O', '-', '-'}};
      char currentplayer = 'X';
      boolean result = Lab3.isWinner(table,currentplayer);
      assertEquals(false, result);
         
      }
      @Test
      public void checktie_Output_True(){
      char[][] table = {{'O', 'O', 'X'}, 
                       {'X', 'X', 'O'}, 
                        {'O', 'X', 'X'}};
      boolean result = Lab3.isTie(table);
      assertEquals(true, result);
         
      }
        @Test
      public void checktie_Output_False(){
      char[][] table = {{'x', 'x', 'X'}, 
                       {'-', 'o', 'O'}, 
                        {'-', '-', '-'}};
      boolean result = Lab3.isTie(table);
      assertEquals(false, result);
         
      }
      }

